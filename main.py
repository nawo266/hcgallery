from mainwindow import Ui_MainWindow
from dialog import Ui_Dialog
from webEngine import  Ui_WebDialog
from PySide6 import QtCore, QtGui, QtWidgets
from PySide6 import QtWebEngineWidgets
import sys
import os
import pathlib
from domonic import *
translate = QtCore.QCoreApplication.translate
application_path = os.path.dirname(sys.executable)


class UI_Window(QtWidgets.QMainWindow):
    def __init__(self):
        super(UI_Window, self).__init__()
        self.ui = Ui_MainWindow()
        self.dial=Ui_Dialog()
        self.web=Ui_WebDialog()
        self.setAcceptDrops(True)
        self.ui.setupUi(self)
        self.code = []

        #code dialog
        self.Dialog = QtWidgets.QDialog()
        self.dialUi = self.dial
        self.dialUi.setupUi(self.Dialog)

        #web engine dialog
        self.webDialog = QtWidgets.QDialog()
        self.webUi = self.web
        self.webUi.setupUi(self.webDialog)

        #radio buttons
        self.ui.radioCode.toggled.connect(self.ui.classEdit.setDisabled)
        self.ui.radioClass.toggled.connect(self.ui.classEdit.setEnabled)
        self.ui.radioCusCSS.toggled.connect(self.ui.CSSEditor.setEnabled)
        self.ui.radioDefCSS.toggled.connect(self.ui.CSSEditor.setDisabled)

        #buttons
        self.ui.listBox.button(self.ui.listBox.Reset).clicked.connect(self.ui.itemList.clear)
        self.ui.listBox.button(self.ui.listBox.Discard).clicked.connect(self.remButton)
        self.ui.generateButton.clicked.connect(self.genGallery)
        self.ui.genListButton.clicked.connect(self.genList)
        self.ui.viewButton.clicked.connect(self.viewButton)
        self.ui.listBox.button(self.ui.listBox.Open).clicked.connect(self.fileDialogMain)

        # dialog buttons
        self.dial.buttonBox.button(self.dial.buttonBox.Ok).clicked.connect(self.Dialog.close)
        self.dial.buttonBox.button(self.dial.buttonBox.Save).clicked.connect(self.saveCode)

        #CSS stuff
        if os.path.isfile("hclightbox.css"):
            f = open("hclightbox.css", "r")
            self.ui.CSSEditor.appendPlainText(f.read())
        else:
            self.ui.CSSEditor.appendPlainText("/*No CSS file included*/")

        #language also hard coded lol
        self.retranslateUI()



    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        files = [u.toLocalFile() for u in event.mimeData().urls()]
        for f in files:
            ff = os.path.basename(f)
            print(ff)
            self.ui.itemList.addItem(ff)

    def saveCode(self):
        if self.code != []:
            fname = QtWidgets.QFileDialog.getSaveFileName(self, "Zapisz Kod", "", "Plik HTML (*.html)")
            if fname[0]:
                with open(fname[0], "w") as file:
                    for item in self.code:
                        file.write("{}\n".format(item))
            else:
                print("no data")


    def remButton(self):
        index=self.ui.itemList.currentRow()
        if index > -1:
            self.ui.itemList.takeItem(index)
            print(index)

    def viewButton(self):
        if self.code != []:
            with open("temp.html","w") as file:
               # file.write("{}\n".format("<html><head></head><body>"))
                for item in self.code:
                    file.write("{}\n".format(item))
                #file.write("{}\n".format("</body></html>"))
            self.webDialog.show()
            tempfile=("file://"+str(pathlib.Path().resolve())+"/temp.html")
            print(tempfile)
            self.web.HCWebEngine.setUrl(tempfile)
            self.webDialog.exec()

        else:
            print("no data")
            print(pathlib.Path().resolve())


    def fileDialogMain(self):
        fnames = QtWidgets.QFileDialog.getOpenFileNames(self, "Wybierz fotki do galerii", "", "Pliki Obrazów (*.jpg, *.png)")
        if fnames[0]:
            i = 0
            for i in range(len(fnames[0])):
                print(str(fnames[0][i]))
                ff = os.path.basename(fnames[0][i])
                self.ui.itemList.addItem(ff)


    def getItemListData(self):
        # Looping through items
        arrayOfData = []
        for item_index in range(self.ui.itemList.count()):
            # Getting the data embedded in each item from the listWidget
            item_data = self.ui.itemList.item(item_index).data(QtCore.Qt.UserRole)

            # Getting the datatext of each item from the listWidget
            item_text = self.ui.itemList.item(item_index).text()
            print(item_text)
            arrayOfData.append(item_text)
        return (arrayOfData)

    def genList(self):
        self.ui.itemList.clear()
        noi = self.ui.spinBox.value()
        item = self.ui.pathEdit.text()
        print(noi)
        print(item)
        i = 0
        for i in range(noi):
            si = str(i)
            itemp = str(item+si)
            print(itemp)
            self.ui.itemList.addItem(itemp)
        self.ui.pathEdit.clear()


    def genGallery(self):
        self.code = []
        code = []
        image = ""
        spin = self.ui.spinBox.value()
        radioclass = self.ui.radioClass.isChecked()
        cssClass = self.ui.classEdit.text()
        seoTitle=self.ui.titleEdit.text()
        seoAlt=self.ui.altEdit.text()
        itemListData = self.getItemListData()
        if self.ui.radioDefCSS.isChecked():
            if os.path.isfile("hclightbox.css"):
                f=open("hclightbox.css", "r")
                CSS=f.read()
            else:
                CSS="/*No CSS file included*/"
        else:
            CSS=self.ui.CSSEditor.toPlainText()

        if len(itemListData) != 0:
            #print(len(self.getItemListData()))
            path = self.ui.pathEdit.text()
            if path != "":
                print("path: "+path)
            i=0
            j=0
            cols = self.ui.spinBoxCol.value()
            temp = 1
            code.append("<style>"+CSS+"\n</style>")
            code.append("<div class="+self.ui.classEdit.text()+" >")
            for i in range(len(itemListData)):
                if temp == 1:
                    code.append("<div class=hcgallery-row>")
                if path != "":
                    image = path+itemListData[i]
                else:
                    image = itemListData[i]
                if seoTitle != "":
                    seoTitle = self.ui.titleEdit.text()+" "+str(i)
                if seoAlt != "":
                    seoAlt = self.ui.altEdit.text()+" "+str(i)
                if i == 0:
                    prev = 0
                else:
                    prev = i-1
                if i == len(itemListData)-1:
                    next = i
                else:
                    next = i+ #Spójrz sobie na to, to jest pierdolony burdel. Ale działa
                code.append(div(a(img(_class="hcbox_thumb", _alt=seoAlt, _title=seoTitle, _src=image), span(img(_src=image), _class="hcbox_full"), _id="img-"+str(i), _href="#img-"+str(i)), a("Quit",_class="hcbox_close", _href="#void"), a("&lt;",_class="hcbox_prev", _href="#img-"+str(prev)), a("&gt;",_class="hcbox_next", _href="#img-"+str(next)), _class="hcbox"))
                if temp == cols or i == len(itemListData)-1:
                    code.append("</div>")
                    temp = 0
                temp = temp + 1
            code.append("</div>")
            self.code = code
            k = 0
            self.Dialog.show()
            self.dial.plainTextEdit.clear()
            for k in range(len(code)):
                print(code[k])
                self.dial.plainTextEdit.appendPlainText(str(code[k]))
            self.Dialog.exec()

        else:
            print(spin)

    def testButton(self):
        print("clicked")

    def retranslateUI(self):
        _translate = QtCore.QCoreApplication.translate
        self.ui.listBox.button(self.ui.listBox.Reset).setText(QtCore.QCoreApplication.translate("MainWindow",u"Reset"))
        self.ui.listBox.button(self.ui.listBox.Discard).setText(QtCore.QCoreApplication.translate("MainWindow",u"Usuń"))
        self.ui.listBox.button(self.ui.listBox.Open).setText(QtCore.QCoreApplication.translate("MainWindow",u"Otwórz"))
        self.dial.buttonBox.button(self.dial.buttonBox.Save).setText(QtCore.QCoreApplication.translate("MainWindow",u"Zapisz"))



def main():
    app = QtWidgets.QApplication(sys.argv)
    translator = QtCore.QTranslator(app)
    translator.load("translate/pl_PL.qm")
    application = UI_Window()
    application.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()